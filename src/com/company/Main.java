package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        int[] a = {5, 7, 23, 95};
        int[] b = {-1, 14, 31, 40};
        int size = a.length + b.length;
        int[] result = new int[size];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        bubblesort(result);
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }

    }

    public static void bubblesort(int[] size) {
        int n = size.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (size[j] > size[j + 1]) {
                    int temp = size[j];
                    size[j] = size[j + 1];
                    size[j + 1] = temp;
                }
            }
        }
    }
}
